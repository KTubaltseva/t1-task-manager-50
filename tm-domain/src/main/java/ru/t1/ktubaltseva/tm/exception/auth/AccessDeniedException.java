package ru.t1.ktubaltseva.tm.exception.auth;

public class AccessDeniedException extends AbstractAuthException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}