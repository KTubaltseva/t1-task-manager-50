package ru.t1.ktubaltseva.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;
import ru.t1.ktubaltseva.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable final String token) {
        super(token);
    }

    public TaskChangeStatusByIdRequest(@Nullable final String token, @Nullable final String id, @Nullable Status status) {
        super(token);
        this.id = id;
        this.status = status;
    }

}
