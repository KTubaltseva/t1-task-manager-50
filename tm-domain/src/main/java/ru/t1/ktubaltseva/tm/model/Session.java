package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public class Session extends AbstractUserOwnedModel {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "role", columnDefinition = "varchar(30)", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    public Session(@NotNull final User user) {
        this.user = user;
    }

}
