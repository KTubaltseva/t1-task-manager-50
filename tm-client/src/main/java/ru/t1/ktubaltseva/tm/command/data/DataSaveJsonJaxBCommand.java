package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveJsonJaxBRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataSaveJsonJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-json-jaxb";

    @NotNull
    private final String DESC = "Save data to json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE JSON DATA]");
        @NotNull final DataSaveJsonJaxBRequest request = new DataSaveJsonJaxBRequest(getToken());
        getDomainEndpoint().saveDataJsonJaxB(request);
    }

}
