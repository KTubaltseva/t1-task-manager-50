package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.ktubaltseva.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.ProjectDTORepository;
import ru.t1.ktubaltseva.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private IProjectDTORepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @NotNull
    private ITaskDTORepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @NotNull
    @Override
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable TaskDTO resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findOneById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findOneById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultTask;
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable TaskDTO resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findOneById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(userId, resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findOneById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultTask;
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
            @Nullable ProjectDTO project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            entityManager.getTransaction().begin();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
            @NotNull final List<ProjectDTO> projects = projectRepository.findAll(userId);
            entityManager.getTransaction().begin();
            for (@NotNull final ProjectDTO project : projects) {
                taskRepository.removeAllByProjectId(userId, project.getId());
            }
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
