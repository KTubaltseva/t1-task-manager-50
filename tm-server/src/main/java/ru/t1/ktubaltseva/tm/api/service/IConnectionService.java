package ru.t1.ktubaltseva.tm.api.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    EntityManager getEntityManager();

    @NotNull
    @SneakyThrows
    EntityManagerFactory getEntityManagerFactory();

    @NotNull
    @SneakyThrows
    Liquibase getLiquibase();

}
